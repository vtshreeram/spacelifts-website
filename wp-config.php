<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'spacelifts_site');

/** MySQL database username */
define('DB_USER', 'spacelifts_site');

/** MySQL database password */
define('DB_PASSWORD', '=n6opkU?g9%B');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Qa4@e$vxGZ;F!G1HR  `$AcG%3:kh+^h1_pIn_mCxAU$GbM;AeH UT;BI:YDtU*A');
define('SECURE_AUTH_KEY',  '|HU1.G_kaHg$(3}Ag|%s!T8;1=HWcnYusThQ1da=t(({uYleC}nECVBNmRz;V/<`');
define('LOGGED_IN_KEY',    '?r}?P381^r<WA,YZtd@Y+RhfQGLaoWEp*DTN4(>PW8kEocL$6KYsu).$hgTLCu9K');
define('NONCE_KEY',        'q{&nR]u4nF^{=<oMG{Dh-1*7q*)N5NJ{ivP8O4=}`|[@F8roXCD[j;LW(YtIGk-D');
define('AUTH_SALT',        '3Sb >UAL/:{KwavQTByyB# eZt}7un`lD#Y`Yr{Mh!(<V8jT6B-!Nzkj~4HqyNux');
define('SECURE_AUTH_SALT', 'k6_W2PAFg,ZR&8Cg5.]Xq%Is)rgVht`p=Im/J>Q~w;lG:~lT =$H nnUzD!{-A*x');
define('LOGGED_IN_SALT',   '~T*<nsLK_$F[.n6-Cik/Q++P0GM5)wQv=QzbnYbCKj_7<R_JBU%F^%bn)Sm=*IEM');
define('NONCE_SALT',       '^0n@kj*3p5TULYhA&N|+|lW!HeZ*8IL)u;G5k#:q&Jl*`@.WJ3AE0*>5B9Cj!ndm');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
